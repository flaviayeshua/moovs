//
//  MovieDetailViewController.m
//  mymovie
//
//  Created by Flavia Yeshua on 21/02/16.
//  Copyright © 2016 FyTech. All rights reserved.
//

#import "MovieDetailViewController.h"
#import "TrailerTableViewCell/TrailerTableViewCell.h"

@interface MovieDetailViewController ()

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageMovie;
@property (weak, nonatomic) IBOutlet UILabel *labelYear;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;
@property (weak, nonatomic) IBOutlet UILabel *labelRating;
@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;

@property (weak, nonatomic) IBOutlet UITableView *tableViewTrailers;

@end

@implementation MovieDetailViewController

static NSString *movieId;

NSArray *totalTrailers;
NSDictionary *quicktime;
NSDictionary *youtube;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadMovieData];
}

- (void)FillOutlets {
    
    self.labelTitle.text = movieData[@"title"];
    
    NSString *year = movieData[@"release_date"];
    
    if (year && ![year isEqualToString:@""] ) {
        
        self.labelYear.text = [year substringWithRange:NSMakeRange(0, 4)];
    }
    
    long time = [movieData[@"runtime"] longValue];
    self.labelTime.text =  [[NSString alloc] initWithFormat: @"%ldmin",time];
    
    float rating = [movieData[@"vote_average"] floatValue];
    self.labelRating.text = [NSString stringWithFormat:@"%.02f/10", rating];
    
    self.labelDescription.text = movieData[@"overview"];

    //build image path
    NSString *urlImage = [[NSString alloc] initWithFormat: @"%@%@",kImageUrlBase, movieData[@"poster_path"]];
    NSData *imgData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString: urlImage]];
    self.imageMovie.image = [UIImage imageWithData:imgData];
    
    //Tratar para adicionar o número total de registros
    totalTrailers = movieData[@"trailers"];
}

- (void) initWithMovieId:(NSString *)initMovieId {
    movieId = initMovieId;
}

-(void)loadMovieData {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [API doGetMovie:movieId block:^(NSDictionary *response) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (response) {
            movieData = response;
            [self FillOutlets];
            [self.tableViewTrailers reloadData];
            
        } else {
            //Show alert
            NSLog(@"Erro on load movies");
        }
        
    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return totalTrailers.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    long index = indexPath.row;
    
    TrailerTableViewCell *trailerCell = [tableView dequeueReusableCellWithIdentifier:@"TrailerCell" forIndexPath:indexPath];
    
    //trailerCell.labelTrailerTitle.text = youtube[@"name"];
    trailerCell.labelTrailerTitle.text = @"Trailler Name";
    
    return trailerCell;
}

@end
