//
//  TraillerTableViewCell.h
//  mymovie
//
//  Created by Flavia Yeshua on 21/02/16.
//  Copyright © 2016 FyTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrailerTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelTrailerTitle;

@property (weak, nonatomic) IBOutlet UIButton *btnTrailerPlay;

@end
