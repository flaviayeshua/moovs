//
//  API.h
//  mymovie
//
//  Created by Flavia Yeshua on 21/02/16.
//  Copyright © 2016 FyTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface API : NSObject

+(void)doListMovies:(NSString *)param block:(void (^)(NSDictionary *))block;
+(void)doGetMovie:(NSString *)idMovie block:(void (^)(NSDictionary *))block;

@end
