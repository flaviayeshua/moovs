//
//  API.m
//  mymovie
//
//  Created by Flavia Yeshua on 21/02/16.
//  Copyright © 2016 FyTech. All rights reserved.
//

#import "API.h"
#import <AFNetworking/AFHTTPSessionManager.h>

@implementation API

#pragma mark - API Methods

+(void)doListMovies:(NSString *)param block:(void (^)(NSDictionary *))block {
    NSString *path = [NSString stringWithFormat:@"discover/movie?%@&sort_by=%@", kApiKey, param];
    
    [self makeRequestWithPath:path parameters:nil requestType:GET completion:block];
}

+(void)doGetMovie:(NSString *)movieId block:(void (^)(NSDictionary *))block {

    NSString *path = [NSString stringWithFormat:@"movie/%@?%@&append_to_response=trailers", movieId, kApiKey];

    [self makeRequestWithPath:path parameters:nil requestType:GET completion:block];
}


#pragma mark - Http Configs

+(void)makeRequestWithPath:(NSString *)path parameters:(id)parameters requestType:(REQUEST_TYPE)requestType completion:(void(^)(id))block {
    NSString *url = [NSString stringWithFormat:kApiBase, path];
    NSURL *request = [NSURL URLWithString:url];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:request];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setTimeoutInterval:25];
    
    // Define Success block
    void (^successBlock) (NSURLSessionDataTask *task, id responseObject) = ^(NSURLSessionDataTask *task, id responseObject) {
        NSError *parseError;
        
        id json = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:&parseError];
        
        if (!parseError) {
            block(json);
        } else {
            block(nil);
        }
    };
    
    // Failure block
    void (^failureBlock) (NSURLSessionDataTask *task, NSError *error) = ^(NSURLSessionDataTask *task, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RequestError" object:nil userInfo:nil];
        
        NSLog(@"Error to request\nURL: %@\nError message: %@", url, [error localizedDescription]);
        NSLog(@"%@", error);
        
        block(nil);
    };
    
    // Request Types
    if (requestType == POST) {
        [manager POST:url parameters:parameters success:successBlock failure:failureBlock];
    } else if (requestType == GET) {
        [manager GET:url parameters:parameters success:successBlock failure:failureBlock];
    }
}

@end
