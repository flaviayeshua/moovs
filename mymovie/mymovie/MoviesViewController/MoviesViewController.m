//
//  MoviesViewController.m
//  mymovie
//
//  Created by Flavia Yeshua on 21/02/16.
//  Copyright © 2016 FyTech. All rights reserved.
//

#import "MoviesViewController.h"
#import "MovieCell.h"

@interface MoviesViewController ()

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentMovies;
@property (weak, nonatomic) IBOutlet UITableView *tableViewMovies;

@end

@implementation MoviesViewController

static  NSString  *  const MOVIES_TITLE =  @"moovs";
static  NSString  *  const MORE_POPULARITY =  @"popularity.desc";
static  NSString  *  const BEST_SCORED =  @"vote_average.desc";

static int segmentOption = 0;
static NSString *movieFilterParam;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureNavigationBar];
    [self prepareSegmentMovies];
    [self loadMovies];
    
    [self.tableViewMovies reloadData];
}

-(void)configureNavigationBar {
    
    self.title = MOVIES_TITLE;
}

-(void)prepareSegmentMovies{
    
    if (segmentOption == 0) {
        self.segmentMovies.selectedSegmentIndex = 0;
        movieFilterParam = MORE_POPULARITY;
    } else {
        self.segmentMovies.selectedSegmentIndex = 1;
        movieFilterParam = BEST_SCORED;
    }
}


#pragma mark - Action

- (IBAction)segment_click:(id)sender {
    
    if (self.segmentMovies.selectedSegmentIndex == 0) {
        segmentOption = 0;
        movieFilterParam = MORE_POPULARITY;
    } else {
        segmentOption = 1;
        movieFilterParam = BEST_SCORED;
    }
    
    [self loadMovies];
    [self.tableViewMovies reloadData];
}

//Acrescentar page
-(void)loadMovies {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [API doListMovies:movieFilterParam block:^(NSDictionary *response) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (response) {
            movieList = response[@"results"];
            
            [self.tableViewMovies reloadData];
            } else {
                //Show alert
                NSLog(@"Erro on load movies");
            }
    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return movieList.count/2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    long index = indexPath.row;
    long firstIndex;
    long secondIndex;
    
    if (index == 0) {
        firstIndex = index;
        secondIndex = index + 1;
    } else{
        firstIndex = index * 2;
        secondIndex = index * 2 +1;
    }
    
    MovieCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSArray *collectionData = @[movieList[firstIndex], movieList[secondIndex]];
    
    [cell initWithCollectionData: collectionData];
    [cell.collectionMovie reloadData];
    
    return cell;
}

@end
