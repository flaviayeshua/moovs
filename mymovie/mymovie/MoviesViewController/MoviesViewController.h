//
//  MoviesViewController.h
//  mymovie
//
//  Created by Flavia Yeshua on 21/02/16.
//  Copyright © 2016 FyTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoviesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>{

    NSArray *movieList;
}


@end
