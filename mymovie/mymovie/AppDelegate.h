//
//  AppDelegate.h
//  mymovie
//
//  Created by Flavia Yeshua on 20/02/16.
//  Copyright © 2016 FyTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

