//
//  MovieCell.m
//  mymovie
//
//  Created by Flavia Yeshua on 21/02/16.
//  Copyright © 2016 FyTech. All rights reserved.
//

#import "MovieCell.h"
#import "MovieCollectionViewCell.h"
#import "MovieDetailViewController.h"

@implementation MovieCell

- (void)awakeFromNib {

    _collectionMovie.delegate = self;
    _collectionMovie.dataSource = self;
}

-(void)initWithCollectionData:(NSArray *)data {
    
    movieCollectionData = data;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Collection view data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return movieCollectionData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    MovieCollectionViewCell *collectionCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];

    NSDictionary *movie = movieCollectionData[indexPath.item];
    NSString *urlImage = [[NSString alloc] initWithFormat: @"%@%@",kImageUrlBase, movie[@"poster_path"]];
    NSData *imgData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString: urlImage]];
    
    collectionCell.movieImage.image = [UIImage imageWithData:imgData];
    
    return collectionCell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    int index = (int)indexPath.item;
    
    MovieDetailViewController *detailController = [[MovieDetailViewController alloc] init];
    [detailController initWithMovieId: movieCollectionData[index][@"id"]];
    
    [self.window.rootViewController.navigationController pushViewController:detailController animated:YES];
}


@end
