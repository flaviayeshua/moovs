//
//  MovieCell.h
//  mymovie
//
//  Created by Flavia Yeshua on 21/02/16.
//  Copyright © 2016 FyTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>{

    NSArray *movieCollectionData;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionMovie;

-(void)initWithCollectionData:(NSArray *)data;

@end
